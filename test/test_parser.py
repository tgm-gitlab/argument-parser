from unittest import TestCase, main
from unittest.mock import patch
import sys
from parameterized import parameterized

from simple_kwarg_parser import read_args, assign


class SimpleKwargParserUnitTestSuite(TestCase):
    @patch.object(sys, 'argv', ['prog', 'arg=1'])
    def test_single_argument(self):
        parameters = {
            'arg': lambda val: assign('arg', val)
        }
        actual = read_args(parameters)
        self.assertTrue('arg' in actual)
        self.assertEqual('1', actual['arg'])

    @patch.object(sys, 'argv', ['prog', 'undef=unknown'])
    def test_ignores_undefined_arguments(self):
        parameters = {
            'arg': lambda val: assign('arg', val)
        }
        actual = read_args(parameters)
        self.assertFalse('arg' in actual)

    @parameterized.expand([
        (['name=bob', 'job=worker'], [('name', 'bob'), ('job', 'worker')]),
        (['name=bob', 'job=worker', 'paid_over_time=false'],
         [('name', 'bob'), ('job', 'worker'), ('paid_over_time', False)]),
        (['name=bob', 'job=worker', 'paid_over_time=false', 'unknown=none'],
         [('name', 'bob'), ('job', 'worker'), ('paid_over_time', False)]),
    ])
    def test_multiple_arguments(self, args, validation_paris):
        parameters = {
            'name': lambda val: assign('name', val),
            'job': lambda val: assign('job', val),
            'paid_over_time': lambda val: assign('paid_over_time', val.lower() == 'true')
        }
        argv = ['prog']
        argv.extend(args)
        with patch.object(sys, 'argv', argv):
            actual = read_args(parameters)
        
        for key, value in validation_paris:
            self.assertEqual(value, actual[key])
        
        


if __name__ == '__main__':
    main()
