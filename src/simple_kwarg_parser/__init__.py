from .parser import read_args, assign

__all__ = ['read_args', 'assign']
