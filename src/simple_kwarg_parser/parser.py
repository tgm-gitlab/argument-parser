import sys

arguments = {}


def assign(key, val):
    arguments[key] = val


def read_args(keyword_assignments):
    global arguments
    arguments = {}

    for i in range(1, len(sys.argv)):
        try:
            passed_arg = sys.argv[i]
            arg, value = passed_arg.split('=', 2)
            if value and arg in keyword_assignments:
                keyword_assignments[arg](value)
        except ValueError:
            pass
    return arguments
