# Simple Kwarg Parser
This parser read arguments in key-value pairs and returns the result. The format is similar to python kwargs.

## Installation
```
pip install simple-kwarg-parser
```

## Usage

To use the parser, you need to define an object describing your expected parameters and define how they are to be assigned.

```python
from simple_kwarg_parser import read_args, assign

parameter_specification = {
    'arg': lambda val: assign('arg', val)
}

kwargs = read_args(parameter_specification)

```

Arguments not defined in the parameter specification will be ignored. In the above example the `assign` function is used to assign whatever is passed in the command line to the kwargs. The first parameter is the key used in the resulting kwargs and it does not need to match supplied in the command line. The second argument is what to assign to that key, in this case simply passing the value from the command line. However, any sort of formatting can be undertaken in this step.

Format the passed value as boolean:
```python
assign('arg', val.lower() == 'true')
```

Any formatting can be done at this step, however the values read from the command line are always strings.